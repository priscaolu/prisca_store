## Author: Prisca Olumoya


---

## Project Name
***Prisca store
Creating an order page using Javascript, html and css. Customers can order online and get an instant receipt.

## Table of Contents
1. [General Info](#general-info)
2. [Technologies](#technologies)
3. [Installation](#installation)
4. [Collaboration](#collaboration)
5. [FAQs](#faqs)

## Table of Contents
<a name="general-info"></a>
### General Info
The Project is an online order kiosk for ordering goods.
Project status: completed

---

## Technologies
***
A list of technologies used within the project:
* [javascript, nodes.js](https://prisca.com): Version 12.3 

## Installation
***
A little intro about the installation. 
install by starting the terminal
```
$ git clone https://prisca.com
$ cd ../path/to/the/file
$ npm install
$ npm start
```

## Collaboration
***
Provide instructions on how to collaborate with your project.
I collaborated with Olatunde 

## FAQs
***
A list of frequently asked questions
1. **How do i order?**
npm start and access through local host
2. __How to make changes to the quantity?__ 
:go to index.js
